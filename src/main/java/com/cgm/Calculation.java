package com.cgm;

public class Calculation {

    //Subtrahiert 2 Zahlen
    public Integer subtraction(Integer minuend, Integer subtrahend)
    {
        return minuend - subtrahend;
    }


    /*
    Berechnet die Addition von zwei Zahlen.
    * */
    public int addition(Integer first, Object second){

        int result = first + (int)second;
        return result;
    }


    // Berechnet die Division zweier Zahlen
    public double division(int dividend, int divisor){
        double result;
        if(divisor == 0) {
            throw new IllegalArgumentException("Divisor must not be null!");
        }

        result = (double)dividend / divisor;

        return result;

    }


    // Berechnet die Quadratwurzel einer Zahl
    public double square(int x) {
        double result = Math.sqrt(x);
        return result;
    }


    //Berechnet den Rest einer Division
    public int modulo(int number, int divisor) {
        return number % divisor;
    }

    // Berechnet die Multiplikation einer Zahl
    public int multiplication(int operand1, int operand2) {

        int result = operand1 * operand2;
        return result;

    }
}
