package com.cgm;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;


public class CalculationTest {

    @Test
    void Calculation_TwoArguments_IsSum()
    {
        //Aggregate
        int first = 1;
        int second = 1;

        //Act
        Calculation current = new Calculation();
        int actual = current.addition(first,second);

        Assertions.assertEquals(2,actual);
    }

    @Test
    void Calculation_ArgumentTwoIsObject_IsSum()
    {
        //Aggregate
        int first = 1;
        Object second = 3;

        //Act
        Calculation current = new Calculation();
        int actual = current.addition(first,second);

        int expected = 4;
        Assertions.assertEquals(expected,actual);
    }

    @Test
    void Subtraction() {
        assertThat(new Calculation().subtraction(5, 3)).isEqualTo(2);
        assertThat(new Calculation().subtraction(-4, 100)).isEqualTo(-104);
        assertThat(new Calculation().subtraction(-300, -100)).isEqualTo(-200);
    }

    @Test
    void division_TestRegularDivisionWith2_ResultIsOK() {

        double result;
        Calculation calc = new Calculation();

        result = calc.division(4, 2);

        assertThat(result).isEqualTo(2);
    }

    // Testet die Funktion square 4
    @Test
    void square_TestSquareRootWith4_ResultIsOK() {
        int x = 4;
        double result;

        Calculation calc = new Calculation();
        result = calc.square(x);

        assertThat(result).isEqualTo(2);
    }


    @Test
    void moduloOperation() {
        assertThat(new Calculation().modulo(5, 3)).isEqualTo(2);
        assertThat(new Calculation().modulo(5, 5)).isEqualTo(0);
        assertThat(new Calculation().modulo(7, 5)).isEqualTo(2);
        assertThat(new Calculation().modulo(9, 3)).isEqualTo(0);
        assertThat(new Calculation().modulo(100, 23)).isEqualTo(8);
    }

    @Test
    void moduloOperation2() {
        assertThat(new Calculation().modulo(5, 3)).isEqualTo(2);
        assertThat(new Calculation().modulo(5, 5)).isEqualTo(0);
        assertThat(new Calculation().modulo(7, 5)).isEqualTo(2);
        assertThat(new Calculation().modulo(9, 3)).isEqualTo(0);
        assertThat(new Calculation().modulo(100, 23)).isEqualTo(8);
    }

    @Test
    void calculation_Multiplication_TwoIntegersAreMultiplied() {
        int op1 = 3;
        int op2 = 5;

        Calculation current = new Calculation();
        int actual = current.multiplication(op1, op2);

        int expected = 15;

        assertThat(actual).isEqualTo(expected);
    }

    // Testet die Funktion square
    @Test
    void square_TestSquareRootWith16_ResultIsOK() {
        int x = 16;
        double result;

        Calculation calc = new Calculation();
        result = calc.square(x);

        assertThat(result).isEqualTo(4);

    }
}
